##############
# Instruction:

Steps for  running our application locally are:
### 1.After creating your new Cloud9 environment in the bash command line type next commands to download source code

`~/workspace $ git clone https://ghajduk1@bitbucket.org/jez12345/app.git` ///download source code of the app
`~/workspace $ cd app/docs/WWS`
`~/workspace/app/docs/WWS(master) $ npm install` ///to start express application

### 2.MongoDB is installed and to host local database next command should be executed,if there are any problems with mongo execute next commands:

```sh
------------------------create local db again ----------------------------------------
$ sudo apt-get remove mongodb-org mongodb-org-server
$ sudo apt-get autoremove
$ sudo rm -rf /usr/bin/mongo*
$ sudo rm /etc/apt/sources.list.d/mongodb*.list
$ sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 2930ADAE8CAF5059EE73BB4B58712A2291FA4AD5
$ echo "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.6 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.6.list
$ sudo apt-get update
$ sudo apt-get install mongodb-org mongodb-org-server
$ sudo touch /etc/init.d/mongod
$ sudo apt-get install mongodb-org-server
------------------------------------------------------------------------------------
~/workspace $ mkdir mongodb
~/workspace $ cd mongodb

~/workspace/app/docs/mongodb $ mkdir data
~/workspace/app/docs/mongodb $ echo 'mongod --bind_ip=$IP --dbpath=data --nojournal "$@"' > mongod
~/workspace/app/docs/mongodb $ chmod a+x mongod

IF ./mongod doesnt run,type ./mongod --repair
------------------------------------------------------------------------------------------------------

~/workspace/app/docs/mongodb $ ./mongod
```

After hosting the local database importing the data in the database is done with executing next command in the new
terminal(alt + T) and database should be left to serve in the previous terminal where you have executed  these commands:

`~/workspace/app/docs/mongodb $ mongo`  ////LEAVE THIS TERMINAL OPEN AND DATABASE SERVING



# import data into the database

### Test data can be deleted or imported in `/db` page.

or

```sh
------------optional--------------------------

First:
~/workspace $ cd app/docs/mongodb

To import test data:
mongoimport --db wwsfri --collection unis --mode upsert --upsertFields name --jsonArray --file ~/workspace/app/docs/WWS/app_server/models/uni.json
mongoimport --db wwsfri --collection users --mode upsert --upsertFields name --jsonArray --file ~/workspace/app/docs/WWS/app_server/models/user.json
mongoimport --db wwsfri --collection feedbacks --mode upsert --upsertFields name --jsonArray --file ~/workspace/app/docs/WWS/app_server/models/feedback.json
```


### 3.In order to start the app,in the new terminal(alt + T) execute following command:
`~/workspace (master) $ cd ~/workspace/app/docs/WWS`
`~/workspace/app/docs/WWS (master) $ npm start` or `nodemon`

### 4.To live preview application in cloud9 on button preview select live preview running application



--------------------------------------EXPLANATION OF THE STRUCTURE OF THE APP_CLIENT DIR-------------------------
# Our application is not dependent on the server and it has direct access to REST_API as it can be seen from the services where it's seen how we call METHODS that `get`,`put` and `delete` data through REST_API.
# In the controllers we manipulating with data that are needed by services.
# Directives are rendering navbar.
# We have implemented all functionalities needed for using IIFE "pristup" with *.$inject in all directives,services,controllers and in app.js
# Our application has more than three Angular views implemented.
# Optimized usage of the app in the broweser is implemented.
# Navigation is working and using back and forward you can move through the application,and all URLs are working properly.
# You can  filter feedbacks.
# Modal window is implemented on the home page after pressing the button for adding new university and there you can add new one to the list of the already existing universities.
 





