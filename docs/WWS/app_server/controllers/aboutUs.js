/* GET about us page */
var aboutUsJSON = require("../models/aboutUs.json");

module.exports.aboutUs = function(req, res) {
  res.render('aboutUs', aboutUsJSON);
};
