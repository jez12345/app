/* GET about us visitors page */
var aboutUsVisitorsJSON = require("../models/aboutUsVisitors.json");

module.exports.aboutUsVisitors = function(req, res) {
  res.render('aboutUsVisitors', aboutUsVisitorsJSON);
};
