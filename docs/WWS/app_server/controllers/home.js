var request = require('request');
var apiParams = {
  server: 'http://localhost:' + process.env.PORT
};
if (process.env.NODE_ENV === 'production') {
  apiParams.server = 'https://wwsfri.herokuapp.com';
}

module.exports.addNewUni = function(req, res) {
  var path = apiParams.server + '/api/newUni';
  var dataToSend = {
    uniName: req.body.uniName,
    country: req.body.country,
    address: req.body.address,
  };
  var reqParams = {
    url: path,
    method: 'POST',
    json: dataToSend
  };
  request(
    reqParams,
    function(error, response, content) {
      res.redirect('/home');
    }
  );
};

module.exports.getAllUnis = function(req, res) {
  var path = '/api/allUnis';
  var reqParams = {
    url: apiParams.server + path,
    method: 'GET',
  };
  request(
    reqParams,
    function(error, response, content) {
      if (!error || error.statusCode === 201) {
        res.render('home', {
          title: "HOME",
          unis: JSON.parse(content)
        });
      }
      else {
        res.render('error', error);
      }
    }
  );
};

module.exports.uniPage = function(req, res) {
  var path = '/api/allUnis';
  var reqParams = {
    url: apiParams.server + path,
    method: 'GET',
  };
  request(
    reqParams,
    function(error, response, content) {
      if (!error || error.statusCode === 201) {
        res.render('uni', {
          title: "DELETE UNI",
          unis: JSON.parse(content)
        });
      }
      else {
        res.render('error', error);
      }
    }
  );
};

module.exports.deleteUni = function(req, res) {
  var idUni = req.body._id;
  var path = '/api/uni/' + idUni;
  var reqParams = {
    url: apiParams.server + path,
    method: 'DELETE',
    json: {},
  };
  request(
    reqParams,
    function(error, response, content) {
      if (!error || error.statusCode === 201) {
        res.redirect('/home/uni');
      }
      else {
        res.render('error', error);
      }
    }
  )
};
