/* GET home visitors page */
module.exports.homeVisitors = function(req, res) {
  res.render('homeVisitors', {
    title: 'HOME VISITORS'
  });
};
