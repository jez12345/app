var request = require('request');
var apiParams = {
  server: 'http://localhost:' + process.env.PORT
};
if (process.env.NODE_ENV === 'production') {
  apiParams.server = 'https://wwsfri.herokuapp.com';
}


/* GET register page */
module.exports.register = function(req, res) {
  var path = '/api/allUnis';
  var reqParams = {
    url: apiParams.server + path,
    method: 'GET',
  };
  request(
    reqParams,
    function(error, response, content) {
      if (!error || error.statusCode === 201) {
        res.render('register', {
          title: "REGISTER",
          unis: JSON.parse(content)
        });
      }
      else {
        res.render('error', error);
      }
    }
  );
};

module.exports.getAllUsers = function(req, res) {
  var path = '/api/register';
  var reqParams = {
    url: apiParams.server + path,
    method: 'GET',
    json: {},
  };
  request(
    reqParams,
    function(error, response, content) {
      if (!error || error.statusCode === 201) {
        res.render('myprofile', {
          title: "Profiles",
          users: content
        });
      }
      else {
        res.render("error", error);
      }
    }
  );
};

module.exports.registerUser = function(req, res) {
  var path = apiParams.server + "/api/register";
  var dataToSend = {
    name: req.body.name,
    lastname: req.body.lastname,
    mail: req.body.mail,
    pass1: req.body.pass1,
    country: req.body.country,
    uniFrom: req.body.university,
    bday: req.body.bday,
  };
  var reqParams = {
    url: path,
    method: 'POST',
    json: dataToSend
  };
  request(
    reqParams,
    function(error, response, content) {
      res.render('myprofile', content);
      //module.exports.getAllUsers(req, res);
    }
  );
};
