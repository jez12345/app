var request = require('request');
var apiParams = {
  server: 'http://localhost:' + process.env.PORT
};
if (process.env.NODE_ENV === 'production') {
  apiParams.server = 'https://wwsfri.herokuapp.com';
}

/* GET my profile page */
var myProfileJSON = require("../models/myProfile.json");

module.exports.myProfile = function(req, res) {
  res.render('myprofile', myProfileJSON);
};

module.exports.getUserById = function(req, res) {
  var path = '/api/myprofile' + '/' + req.params.idUser;
  var paramsReq = {
    url: apiParams.server + path,
    method: 'GET',
    json: {},
  };
  request(
    paramsReq,
    function(error, response, content) {
      if (!error || error.statusCode === 201) {
        res.render('myprofile', content);
      }
      else {
        res.render('error', error);
      }
    }
  );
};

module.exports.getAllUsers = function(req, res) {
  var path = '/api/allusers';
  var reqParams = {
    url: apiParams.server + path,
    method: 'GET',
  };
  request(
    reqParams,
    function(error, response, content) {
      console.log(content);
      if (!error || error.statusCode === 201) {
        res.render('myprofile', {
          title: "USERS",
          users: JSON.parse(content)
        });
      }
      else {
        res.render('error', error);
      }
    }
  );
};

module.exports.deleteUserById = function(req, res) {
  var idUser = req.body._id;
  var path = '/api/myprofile/' + idUser;
  var reqParams = {
    url: apiParams.server + path,
    method: 'DELETE',
    json: {},
  };
  request(
    reqParams,
    function(error, response, content) {
      if (!error || error.statusCode === 201) {
        res.redirect('/myprofile/allusers');
      }
      else {
        res.render('error', error);
      }
    }
  );
};
