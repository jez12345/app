var request = require('request');
var apiParams = {
  server: 'http://localhost:' + process.env.PORT
};
if (process.env.NODE_ENV === 'production') {
  apiParams.server = 'https://wwsfri.herokuapp.com';
}

module.exports.getAllFeedBacks = function(req, res) {
  var path = '/api/feedback';
  var reqParams = {
    url: apiParams.server + path,
    method: 'GET',
  };
  request(
    reqParams,
    function(error, response, content) {
      if (!error || error.statusCode === 201) {
        res.render('home', {
          title: "Feedbacks",
          feedbacks: JSON.parse(content)
        });
      }
      else {
        res.render('error', error);
      }
    }
  );
};

module.exports.createNewFeedBack = function(req, res) {
  var path = apiParams.server + '/api/feedback';
  var dataToSend = {
    user: {
      fname: req.body.firstname,
      lname: req.body.lastname,
      country: req.body.country,
    },
    suniversity: req.body.suniversity,
    tuniversity: req.body.tuniversity,
    body: req.body.body,
  };
  var reqParams = {
    url: path,
    method: 'POST',
    json: dataToSend
  };
  request(
    reqParams,
    function(error, response, content) {
      module.exports.getAllFeedBacks(req, res);
    }
  );
};

module.exports.deleteFeedBack = function(req, res) {
  var idFeedback = req.body._id;
  var path = '/api/feedback/' + idFeedback;
  var reqParams = {
    url: apiParams.server + path,
    method: 'DELETE',
    json: {},
  };
  request(
    reqParams,
    function(error, response, content) {
      if (!error || error.statusCode === 201) {
        res.redirect('/feedback');
      }
      else {
        res.render('error', error);
      }
    }
  );
};
