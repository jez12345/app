/* GET edit page */
var editJSON = require("../models/edit.json");

module.exports.edit = function(req, res) {
  res.render('edit', editJSON);
};
