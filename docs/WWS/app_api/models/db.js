var mongoose = require('mongoose');

var dbURI = 'mongodb://localhost/wwsfri';
if (process.env.NODE_ENV === 'production') {
  dbURI = process.env.MLAB_URI;
}
mongoose.connect(dbURI, { useNewUrlParser: true, useCreateIndex: true });

mongoose.connection.on('connected', function() {
  console.log('TEXT: Mongoose connected to ' + dbURI);
});

mongoose.connection.on('error', function(err) {
  console.log('TEXT: Mongoose error at connection: ' + err);
});

mongoose.connection.on('disconnected', function() {
  console.log('TEXT: Mongoose connection has been terminated.');
});

var runOK = function(message, callback) {
  mongoose.connection.close(function() {
    console.log('TEXT: Mongoose has been closed on ' + message);
    callback();
  });
};

// Re-run with nodemon
process.once('SIGUSR2', function() {
  runOK('TEXT: nodemon re-run', function() {
    process.kill(process.pid, 'SIGUSR2');
  });
});

// Close
process.on('SIGINT', function() {
  runOK('TEXT: close', function() {
    process.exit(0);
  });
});

// Close on Heroku
process.on('SIGTERM', function() {
  runOK('TEXT: close on Heroku', function() {
    process.exit(0);
  });
});

require('./userMongooseScheme');
