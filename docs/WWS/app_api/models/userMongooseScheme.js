var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var universityShema = new mongoose.Schema({
  uniName: { "type": "string" },
  country: { "type": "string" },
  address: { "type": "string" }
});


mongoose.model('Uni', universityShema, 'unis');
//var university = mongoose.model('university');

var userShema = new mongoose.Schema({
  fname: String,
  lname: String,
  email: String,
  pass: String,
  country: String,
  uniFrom: universityShema,
  hashValue: String,
  nonceValue: String
});

userShema.methods.setPass = function(pass) {
  this.nonceValue = crypto.randomBytes(16).toString('hex');
  this.hashValue = crypto.pbkdf2Sync(pass, this.nonceValue, 1000, 64, 'sha512').toString('hex');
};

userShema.methods.chechPass = function(pass) {
  var hashValue = crypto.pbkdf2Sync(pass, this.nonceValue, 1000, 64, 'sha512').toString('hex');
  return this.hashValue == hashValue;
};

userShema.methods.generateJWT = function() {
  var dateJWT = new Date();
  dateJWT.setDate(dateJWT.getDate() + 7);

  return jwt.sign({
    _id: this._id,
    email: this.email,
    fname: this.fname,
    dateJWT: parseInt(dateJWT.getTime() / 1000, 10)
  }, process.env.JWT_PASS);
};


mongoose.model('User', userShema, 'users');
//var usermodel = mongoose.model('user');

/*
var feedbackShema = new mongoose.Schema({
  user: [userShema],
  uniNow: [universityShema],
  uniFrom: [universityShema],
  body: { "type": "string" },
  createdDate: {
    type: Date,
    "default": Date.now
  }
});
*/

var feedbackShema = new mongoose.Schema({
  user: userShema,
  uniSource: String,
  uniTarget: String,
  body: { "type": "string", "require": true },
  createdDate: {
    type: Date,
    "default": Date.now
  }
});

var feedbacks = mongoose.model('Feedback', feedbackShema);
//var feedback = mongoose.model('feedback');
