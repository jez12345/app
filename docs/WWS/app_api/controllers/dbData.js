var mongoose = require('mongoose');
var user = mongoose.model('User');
var uni = mongoose.model('Uni');
var feedback = mongoose.model('Feedback');
var mongo = mongoose;
var JSONresp = function(res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.addData = function(req, res) {
  mongo.connection.collections['users'].insertMany([{
    "fname": "456",
    "lname": "4567",
    "email": "moj@mail.7",
    "country": "Russia",
    "uniFrom": {
      "uniName": "University of Montenegro",
      "city": "Podgorica",
      "country": "Montenegro",
    },
    "nonceValue": "9fcd6ebcc73h7e1afb96e2f462c6f069",
    "hashValue": "8a1da70438532dab355b259f8aa88d7deff4f16021ed77b24a2833cfa50fe4261248563bd714e661eb8af2025a78019763317c8b22c31563f6a9f5c7356d2f4f"
  }, {
    "fname": "name1",
    "lname": "lastname3",
    "email": "user@mail.1",
    "country": "USA",
    "uniFrom": {
      "uniName": "University of Montenegro",
      "city": "Podgorica",
      "country": "Montenegro",
    },
    "nonceValue": "9fcd6ebcc73d7v1afb96e2f462c6f069",
    "hashValue": "8a1da70438532ddba55b259f8aa88d7deff4f16021ed77b24a2833cfa50fe4261248563bd714e661eb8af2025a78019763317c8b22c31563f6a9f5c7356d2f4f"
  }, {
    "fname": "name2",
    "lname": "lastname2",
    "email": "moj@mail.2",
    "country": "UK",
    "uniFrom": {
      "uniName": "University of Ljubljana",
      "city": "Ljubljana",
      "country": "Slovenia"
    },
    "nonceValue": "9fcd6ebcc73d7e1aab96e2f462c6f069",
    "hashValue": "8a1da70438532ddb35ab259f8aa88d7deff4f16021ed77b24a2833cfa50fe4261248563bd714e661eb8af2025a78019763317c8b22c31563f6a9f5c7356d2f4f"
  }, {
    "fname": "name3",
    "lname": "lastname3",
    "email": "moj@mail.3",
    "country": "Japan",
    "uniFrom": {
      "uniName": "Massachusetts Institute of Technology",
      "city": "Cambridge",
      "country": "USA"
    },
    "nonceValue": "9fcd6ebcc73d7e1afba6e2f462c6f069",
    "hashValue": "8a1da70438532ddb355ba59f8aa88d7deff4f16021ed77b24a2833cfa50fe4261248563bd714e661eb8af2025a78019763317c8b22c31563f6a9f5c7356d2f4f"
  }, {
    "fname": "name4",
    "lname": "lastname4",
    "email": "moj@mail.4",
    "country": "Montenegro",
    "uniFrom": {
      "uniName": "Oxford",
      "city": "Oxford",
      "country": "UK"
    },
    "nonceValue": "9fcd6ebcc73d7e1afb96a2f462c6f069",
    "hashValue": "8a1da70438532ddb355b25af8aa88d7deff4f16021ed77b24a2833cfa50fe4261248563bd714e661eb8af2025a78019763317c8b22c31563f6a9f5c7356d2f4f"
  }, {
    "fname": "name5",
    "lname": "lastname5",
    "email": "moj@mail.5",
    "country": "Slovenia",
    "uniFrom": {
      "uniName": "Moscow State University",
      "city": "Moscow",
      "country": "Russia"
    },
    "nonceValue": "9fcd6ebcc73d7e1afb96e2a462c6f069",
    "hashValue": "8a1da70438532ddb355b2598aaa88d7deff4f16021ed77b24a2833cfa50fe4261248563bd714e661eb8af2025a78019763317c8b22c31563f6a9f5c7356d2f4f"
  }, {
    "fname": "name6",
    "lname": "lastname6",
    "email": "moj@mail.6",
    "country": "Russia",
    "uniFrom": {
      "uniName": "University of Tokyo",
      "city": "Tokyo",
      "country": "Japan"
    },
    "nonceValue": "9fcd6ebcc73d7e1afb96e2f4a2c6f069",
    "hashValue": "8a1da70438532ddb355b259f8aa88d7deff4f16021ed77b24a2833cfa50fe4261248563bd714e661eb8af2025a78019763317c8b22c31563f6a9f5c7356d2f4f"
  }]);
  mongo.connection.collections['unis'].insertMany([
    {
      "uniName": "University of Montenegro",
      "city": "Podgorica",
      "country": "Montenegro"
  },
    {
      "uniName": "University of Ljubljana",
      "city": "Ljubljana",
      "country": "Slovenia"
  },
    {
      "uniName": "Massachusetts Institute of Technology",
      "city": "Cambridge",
      "country": "USA"
  },
    {
      "uniName": "Oxford",
      "city": "Oxford",
      "country": "UK"
  },
    {
      "uniName": "Moscow State University",
      "city": "Moscow",
      "country": "Russia"
  },
    {
      "uniName": "University of Tokyo",
      "city": "Tokyo",
      "country": "Japan"
  }
  ]);
  mongo.connection.collections['feedbacks'].insertMany(
    [
      {
        "user": {
          "fname": "1234",
          "lname": "4567",
          "country": "Slovenia"
        },
        "uniSource": "Moscow State University",
        "uniTarget": "University of Ljubljana",
        "body": "123456667"
      },
      {
        "user": {
          "fname": "ime",
          "lname": "prezime",
          "country": "Slovenia"
        },
        "uniSource": "University of Ljubljana",
        "uniTarget": "University of Ljubljana",
        "body": "12567df"
  },
      {
        "user": {
          "fname": "moje ime 5",
          "lname": "4567",
          "country": "UK"
        },
        "uniSource": "University of Ljubljana",
        "uniTarget": "Oxford",
        "body": "12345666"
  },
      {
        "user": {
          "fname": "moje ime",
          "lname": "4567",
          "country": "Montenegro"
        },
        "uniSource": "University of Ljubljana",
        "uniTarget": "Massachusetts Institute of Technology",
        "body": "1222222"
}
]);
};

module.exports.removeData = function(req, res) {
  mongo.connection.collections['users'].drop(
    function(error) {
      console.log("users droped!");
    }
  );
  mongo.connection.collections['unis'].drop(
    function(error) {
      console.log("unis droped!");
    }
  );
  mongo.connection.collections['feedbacks'].drop(
    function(error) {
      console.log("feedbacks droped!");
    }
  );
};
