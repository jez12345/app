var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');

var JSONresp = function(res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.register = function(req, res) {
  if (!req.body.pass) {
    JSONresp(res, 400, {
      "message": "All data is required! pass"
    });
  }
  if (!req.body.email) {
    JSONresp(res, 400, {
      "message": "All data is required! email"
    });
  }
  if (!req.body.country) {
    JSONresp(res, 400, {
      "message": "All data is required! country"
    });
  }
  if (!req.body.lname) {
    JSONresp(res, 400, {
      "message": "All data is required! lname"
    });
  }
  if (!req.body.fname) {
    JSONresp(res, 400, {
      "message": "All data is required! fname"
    });
  }
  if (!(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
      .test(req.body.email))) {
    JSONresp(res, 400, {
      "message": "Email is not correct!"
    });
    return;
  }

  var user = new User();
  user.fname = req.body.fname;
  user.lname = req.body.lname;
  user.email = req.body.email;
  user.country = req.body.country;
  user.setPass(req.body.pass);
  user.save(function(error) {
    if (error) {
      JSONresp(res, 500, error);
    }
    else {
      JSONresp(res, 200, {
        "token": user.generateJWT()
      });
    }
  });
};

module.exports.login = function(req, res) {
  if (!req.body.email || !req.body.pass) {
    JSONresp(res, 400, {
      "message": "All data is required!"
    });
  }
  if (!(/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/
      .test(req.body.email))) {
    JSONresp(res, 400, {
      "message": "Email is not correct!"
    });
    return;
  }
  passport.authenticate('local', function(error, user, data) {
    if (error) {
      JSONresp(res, 404, error);
      return;
    }
    if (user) {
      JSONresp(res, 200, {
        "token": user.generateJWT()
      });
    }
    else {
      JSONresp(res, 401, data);
    }
  })(req, res);
};
