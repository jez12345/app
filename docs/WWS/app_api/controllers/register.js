var mongoose = require('mongoose');
var user = mongoose.model('User');

var JSONresp = function(res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.addUser = function(req, res) {
  user
    .create({
      fname: req.body.name,
      lname: req.body.lname,
      email: req.body.mail,
      pass: req.body.pass1,
      country: req.body.country,
    }, function(error, user) {
      if (error) {
        JSONresp(res, 400, error);
      }
      else {
        JSONresp(res, 201, user);
      }
    });
};
