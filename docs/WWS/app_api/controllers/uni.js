var mongoose = require('mongoose');
var uni = mongoose.model('Uni');

var JSONresp = function(res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.addNewUni = function(req, res) {
  uni
    .create({
      uniName: req.body.uniName,
      country: req.body.country,
      address: req.body.address,
    }, function(error, user) {
      if (error) {
        JSONresp(res, 400, error);
      }
      else {
        JSONresp(res, 201, user);
      }
    });
};

module.exports.getAllUnis = function(req, res) {
  uni
    .find()
    .exec(
      function(error, uni) {
        if (error) {
          JSONresp(res, 500, error);
          return;
        }
        JSONresp(res, 200, uni);
      }
    );
};

module.exports.deleteUniById = function(req, res) {
  var idUni = req.params.idUni;
  if (req.params && req.params.idUni) {
    if (idUni) {
      uni
        .findByIdAndRemove(idUni)
        .exec(
          function(error, uni) {
            if (error) {
              JSONresp(res, 404, error);
              return;
            }
            JSONresp(res, 204, null);
          }
        );
    }
    else {
      JSONresp(res, 400, "Can't find _idUni");
    }
  }
  else {
    JSONresp(res, 400, "No params");
  }
};
