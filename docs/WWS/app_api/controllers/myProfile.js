var mongoose = require('mongoose');
var user = mongoose.model('User');

var JSONresp = function(res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.getUserById = function(req, res) {
  if (req.params && req.params.idUser) {
    user
      .findById(req.params.idUser)
      .exec(
        function(error, user) {
          if (!user) {
            JSONresp(res, 404, "User not found!");
            return;
          }
          else if (error) {
            JSONresp(res, 500, error);
            return;
          }
          JSONresp(res, 200, user);
        });
  }
  else {
    JSONresp(res, 400, "There is no id!");
  }
};

module.exports.getAllUsers = function(req, res) {
  user
    .find()
    .exec(
      function(error, user) {
        if (error) {
          JSONresp(res, 500, error);
          return;
        }
        JSONresp(res, 200, user);
      }
    );
};

module.exports.deleteUserById = function(req, res) {
  var idUser = req.params.idUser;
  if (req.params && req.params.idUser) {
    if (idUser) {
      user
        .findByIdAndRemove(idUser)
        .exec(
          function(error, user) {
            if (error) {
              JSONresp(res, 404, error);
              return;
            }
            JSONresp(res, 204, null);
          }
        );
    }
    else {
      JSONresp(res, 400, "Can't find _idUser");
    }
  }
  else {
    JSONresp(res, 400, "No params");
  }
};
