var mongoose = require('mongoose');
var feedback = mongoose.model('Feedback');
var user = mongoose.model('User');

var JSONresp = function(res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.deleteFeedBackById = function(req, res) {
  var idFeedback = req.params.idFeedback;
  if (req.params && req.params.idFeedback) {
    if (idFeedback) {
      feedback
        .findByIdAndRemove(idFeedback)
        .exec(
          function(error, feedback) {
            if (error) {
              JSONresp(res, 404, error);
              return;
            }
            JSONresp(res, 204, null);
          }
        );
    }
    else {
      JSONresp(res, 400, "Can't find _idFeedback");
    }
  }
  else {
    JSONresp(res, 400, "No params");
  }
};

module.exports.getAllFeedBacks = function(req, res) {
  feedback
    .find()
    .exec(
      function(error, user) {
        if (error) {
          JSONresp(res, 500, error);
          return;
        }
        JSONresp(res, 200, user);
      }
    );
};

module.exports.addNewFeedback = function(req, res) {
  feedback
    .create({
      user: req.body.user,
      uniSource: req.body.suniversity,
      uniTarget: req.body.tuniversity,
      body: req.body.body,
    }, function(error, feedback) {
      if (error) {
        JSONresp(res, 400, error);
      }
      else {
        JSONresp(res, 201, feedback);
      }
    });
};

/*
var returnAuthor = function(req, res, callBack) {
  if (req.payload && req.payload.email) {
    user
      .findOne({
        email: req.payload.email
      })
      .exec(function(error, user) {
        if (!user) {
          JSONresp(res, 404, {
            "message": "Cannot find user!"
          });
          return;
        }
        else if (error) {
          JSONresp(res, 500, error);
          return;
        }
        callBack(req, res, user.name);
      });
  }
  else {
    JSONresp(res, 400, {
      "message": "No data available!"
    });
    return;
  }
};*/
