var passport = require('passport');
var localStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var user = mongoose.model('User');

passport.use(new localStrategy({
    usernameField: 'email',
    passwordField: 'pass'
  },
  function(email, pass, final) {
    user.findOne({
        email: email
      },
      function(error, user) {
        if (error)
          return final(error);
        if (!user) {
          return final(null, false, {
            message: 'Bad username'
          });
        }
        if (!user.chechPass(pass)) {
          return final(null, false, {
            message: 'Bad password'
          });
        }
        return final(null, user);
      }
    );
  }
));
