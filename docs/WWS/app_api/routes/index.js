var express = require('express');
var router = express.Router();

var jwt = require('express-jwt');
var authentication = jwt({
  secret: process.env.JWT_PASS,
  userProperty: 'payload'
});

// var ctrlRegister = require('../controllers/register');
var ctrlAllFeedBacks = require("../controllers/feedback");
var ctrlMyProfile = require("../controllers/myprofile");
var ctrlUni = require("../controllers/uni");
var ctrlAuthentication = require('../controllers/authentication');
var ctrlDB = require('../controllers/dbData');

// db
router.delete('/emptyDB', ctrlDB.removeData);
router.post('/addInDb', ctrlDB.addData);


// user methods
// router.post('/register', ctrlRegister.addUser);
router.get('/myprofile/:idUser', ctrlMyProfile.getUserById);
router.get('/allusers', ctrlMyProfile.getAllUsers);
router.delete('/myprofile/:idUser', ctrlMyProfile.deleteUserById);

// feedback methods
router.delete('/feedback/:idFeedback', /*authentication,*/ ctrlAllFeedBacks.deleteFeedBackById);
router.post('/feedBack', /*authentication,*/ ctrlAllFeedBacks.addNewFeedback);
router.get('/feedback', /*authentication,*/ ctrlAllFeedBacks.getAllFeedBacks);

// unis methods
router.delete('/uni/:idUni', ctrlUni.deleteUniById);
router.post('/newUni', ctrlUni.addNewUni);
router.get('/allUnis', ctrlUni.getAllUnis);

// Authentication
router.post('/register', ctrlAuthentication.register);
router.post('/login', ctrlAuthentication.login);


module.exports = router;
