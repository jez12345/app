function validation1() {
    var name = document.getElementById('name').value;
    var lastname = document.getElementById('lastname').value;
    var mail = document.getElementById('mail').value;
    var pass1 = document.getElementById('pass1').value;
    var pass2 = document.getElementById('pass2').value;
    var count = document.getElementById('country').value;
    var regex = /[^A-Za-z]/g;
    var mail_regex = /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/;
    var found1 = name.match(regex);
    var found2 = lastname.match(regex);
    var falseinput = [];
    var i = 0;
    var br = 0;
    var valid = true;

    if (name == "") {
        document.getElementById('name1').innerHTML = "*** Please enter your name";
        falseinput.push("false");
    }
    if (found1 != null) {
        document.getElementById('name1').innerHTML = "*** Your input consist of non letter chars";
        falseinput.push("false");

    }
    if (lastname == "") {
        document.getElementById('lastname1').innerHTML = "*** Please enter your last name";
        falseinput.push('false');
    }
    if (found2 != null) {
        document.getElementById('lastname1').innerHTML = "*** Your input consist of non letter chars";
        falseinput.push("false");

    }
    if (pass1 == "" || pass1.length < 6) {
        document.getElementById('pass_1').innerHTML = "*** Please re-enter your password";
        falseinput.push("false");

    }
    if (pass2 == "" || pass2.length < 6) {
        document.getElementById('pass_2').innerHTML = "*** Please re-enter your password";
        falseinput.push("false");

    }
    if (pass2 != pass1) {
        document.getElementById('pass_2').innerHTML = "**Your passwords don't match";
        falseinput.push("false");
    }
    if (!mail_regex.test(mail) || mail == "") {
        document.getElementById('mail_1').innerHTML = "***Please enter valid e-mail";
        falseinput.push("false");

    }
    if (count == "") {
        document.getElementById("country_name").innerHTML = "**Please enter your country name";
        falseinput.push("false");
    }

    for (i = 0; i < falseinput.length; i++) {
        if (falseinput[i] === "false") {
            valid = false;
            return valid;
        }
    }

    return valid;
}
