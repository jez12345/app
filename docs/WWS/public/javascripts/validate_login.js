function validation() {
  var mail = document.getElementById('user').value;
  var password = document.getElementById("password").value;
  var mail_regex = /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/;
  var falseinput = [];

  var i = 0;
  var br = 0;
  var valid = true;


  if (!mail_regex.test(mail) || mail == "") {
    document.getElementById('mail_1').innerHTML = "***Please enter valid e-mail";
    falseinput.push("false");

  }
  if (password == "" || password.length < 6) {
    falseinput.push('false');
    document.getElementById('passw').innerHTML = "*** Please enter your password ***";
  }

  for (i = 0; i <= falseinput.length; i++) {
    if (falseinput[i] === 'false') {

      valid = false;
      return valid;

    }
  }

  return valid;

}
