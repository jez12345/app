function validation2() {
    var name = document.getElementById('ime').value;
    var lastname = document.getElementById('prezime').value;
    var mail = document.getElementById('mail').value;
    var tel = document.getElementById('tel').value;
    var password = document.getElementById('pass').value;
    var repassword = document.getElementById('repass').value;
    var falseinput = [];
    var reg = true;

    var mail_regex = /^([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x22([^\x0d\x22\x5c\x80-\xff]|\x5c[\x00-\x7f])*\x22))*\x40([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d)(\x2e([^\x00-\x20\x22\x28\x29\x2c\x2e\x3a-\x3c\x3e\x40\x5b-\x5d\x7f-\xff]+|\x5b([^\x0d\x5b-\x5d\x80-\xff]|\x5c[\x00-\x7f])*\x5d))*$/;
    var regex = /[^A-Za-z]/g;
    var tel_regex = /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/g;
    var found = name.match(regex);
    var found1 = lastname.match(regex);
    var i = 0;


    if (name == "") {
        window.alert("Please enter your name!!");
        falseinput.push("false");
    }
    if (found != null) {
        window.alert("Your name contains non letter characters");
        falseinput.push("false");
    }
    if (lastname == "") {
        window.alert("Please enter your last name!!");
        falseinput.push("false");
    }
    if (found1 != null) {
        window.alert("Your last name contains non letter characters");
        falseinput.push("false");
    }
    if (!mail_regex.test(mail) || mail == "") {
        window.alert("Please enter valid e-mail");
        falseinput.push("false");
    }
    if (!tel_regex.test(tel)) {
        window.alert("Please enter valid phone number");
        falseinput.push("false");
    }
    if (password == "" || password.length < 6) {
        window.alert("Please re-enter your password");
        falseinput.push("false");

    }
    if (repassword == "" || repassword.length < 6) {
        window.alert("Please re-enter your password");
        falseinput.push("false");

    }
    if (password != repassword) {
        window.alert("Your passwords do not match");
        falseinput.push("false");
    }
    for (i = 0; i < falseinput.length; i++) {
        if (falseinput[i] === "false") {
            reg = false;
            return reg;
        }
    }
    return reg;
}
