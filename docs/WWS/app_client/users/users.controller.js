(function() {
  function usersCtrl(usersData) {
    var vm = this;

    usersData.data().then(
      function success(response) {
        console.log(response.data);
        vm.msg = response.data.length > 0 ? "" : "No users.";
        vm.data = response.data;
      },
      function error(response) {
        vm.msg = "Error while fetching feedbacks.";
        console.log(response.e);
      });

    vm.userDel = function(userId) {
      if (!userId) {
        vm.formError = "There is something missing. Try it again!";
        alert("Cant find ID. deleteUni");
        return false;
      }
      else {
        vm.execDelId(userId);
      }
    };

    vm.execDelId = function(userId) {
      alert(userId);
      vm.formError = "";
      usersData
        .userId(userId)
        .then(
          function success(res) {
            console.log("BRAVO MALI" + res);
          },
          function error(res) {
            vm.formError = res.data;
          }
        );
    };

  }

  usersCtrl.$inject = ['usersData'];

  /* global angular */
  angular
    .module('wws')
    .controller('usersCtrl', usersCtrl);
})();
