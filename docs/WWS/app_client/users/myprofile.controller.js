(function() {
  function myprofileCtrl($location, authentication) {
    var navvm = this;

    navvm.currentLocation = $location.path();

    navvm.isLogedIn = authentication.isLogedIn();

    navvm.currentUser = authentication.currentUser();
    console.log(navvm.currentUser);

  }

  myprofileCtrl.$inject = ['$location', 'authentication'];

  /* global angular */
  angular
    .module('wws')
    .controller('myprofileCtrl', myprofileCtrl);
})();
