(function() {
  function provider($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'feedbacks/addFeedback.view.html',
        controller: 'uniCtrl',
        controllerAs: 'vm'
      })
      .when('/feedback', {
        templateUrl: 'feedbacks/allFeedbacks.view.html',
        controller: 'feedbackCtrl',
        controllerAs: 'vm'
      })
      .when('/aboutus', {
        templateUrl: 'all/htmls/aboutUs.html',
        controller: '',
        controllerAs: 'vm'
      })
      .when('/home/uni', {
        templateUrl: 'all/htmls/uni.html',
        controller: 'uniCtrl',
        controllerAs: 'vm'
      })
      .when('/myprofile', {
        templateUrl: 'users/myprofile.html',
        controller: 'myprofileCtrl',
        controllerAs: 'vm'
      })
      .when('/myprofile/allusers', {
        templateUrl: 'users/allprofiles.html',
        controller: 'usersCtrl',
        controllerAs: 'vm'
      })
      .when('/register', {
        templateUrl: 'authentication/register.html',
        controller: 'registerCtrl',
        controllerAs: 'vm'
      })
      .when('/login', {
        templateUrl: 'authentication/login.html',
        controller: 'logInCtrl',
        controllerAs: 'vm'
      })
      .when('/db', {
        templateUrl: 'all/htmls/homevisitors.html',
        controller: 'navigationCtrl',
        controllerAs: 'vm'
      })

      .otherwise({ redirectTo: '/' });

    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('');
  }



  /* global angular */
  angular
    .module('wws', ['ngRoute', 'ui.bootstrap'])
    .config(['$routeProvider', '$locationProvider', provider]);

})();
