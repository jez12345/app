(function() {
  function addUniCtrl($uibModalInstance, uniData) {
    var vm = this;

    vm.modalWindiws = {
      close: function() {
        $uibModalInstance.close();
      }
    };

    vm.unisData = {
      uniName: "",
      country: "",
      address: ""
    };

    vm.sendUni = function() {

      vm.err = "";
      if (!vm.unisData.uniName || !vm.unisData.country || !vm.unisData.address) {
        vm.err = "Zahtevani so vsi podatki, prosim poskusite znova!";
        return false;
      }
      else {
        console.log("mrs");
        vm.registr();
      }
    };

    vm.registr = function() {
      vm.err = "";
      uniData
        .addUni(vm.unisData)
        .then(
          function success(res) {
            console.log("BRAVO!" + res);

          },
          function error(res) {
            vm.err = res;
          }
        );
    };
  }

  addUniCtrl.$inject = ['$uibModalInstance', 'uniData'];

  /* global angular */
  angular
    .module('wws')
    .controller('addUniCtrl', addUniCtrl);
})();
