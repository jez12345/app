(function() {
  function registerCtrl($location, uniData, authentication) {
    var vm = this;

    vm.registerData = {
      fname: "",
      lname: "",
      email: "",
      country: "",
      pass: ""
    };


    uniData.data().then(
      function success(response) {
        console.log(response.data + "!!!!!!!!!!!");
        vm.msg = response.data.length > 0 ? "" : "No unis.";
        vm.data = response.data;
      },
      function error(response) {
        vm.msg = "Error while fetching unis.";
        console.log(response.e);
      });

    vm.sendData = function() {
      vm.formError = "";
      if (!vm.registerData.fname || !vm.registerData.lname || !vm.registerData.email || !vm.registerData.country || !vm.registerData.pass) {
        vm.formError = "There is something missing. Try it again!";
        return false;
      }
      else {
        vm.execRegistration();
      }
    };

    vm.execRegistration = function() {
      vm.formError = "";
      authentication
        .register(vm.registerData)
        .then(
          function(success) {
            $location.search('page', null);
            $location.path(vm.indexPage);
          },
          function(error) {
            vm.formError = error.data.message;
          }
        );
    };

    vm.indexPage = $location.search().page || '/';
  }

  registerCtrl.$inject = ['$location', 'uniData', 'authentication'];

  /* global angular */
  angular
    .module('wws')
    .controller('registerCtrl', registerCtrl);
})();
