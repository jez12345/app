(function() {
  function logInCtrl($location, authentication) {
    var vm = this;


    vm.logInData = {
      email: "",
      pass: ""
    };

    vm.indexPage = $location.search().page || '/';

    vm.sendData = function() {
      vm.formError = "";
      if (!vm.logInData.email || !vm.logInData.pass) {
        vm.formError = "There is something missing. Try it again!";
        return false;
      }
      else {
        vm.exacLogIn();
      }
    };

    vm.exacLogIn = function() {
      vm.formError = "";
      authentication
        .login(vm.logInData)
        .then(
          function(success) {
            $location.search('page', null);
            $location.path(vm.indexPage);
          },
          function(error) {
            vm.formError = error.data.message;
          }
        );
    };
  }

  logInCtrl.$inject = ['$location', 'authentication'];

  /* global angular */
  angular
    .module('wws')
    .controller('logInCtrl', logInCtrl);
})();
