(function() {
  function dbService($http) {

    var emptyDBs = function() {
      alert("Sad sam u service!");
      return $http.delete('/api/emptyDB');
    };

    var addDb = function() {
      alert("Sad sam u service addDb!");
      return $http.post('/api/addInDb');
    };

    addDb

    return {
      emptyDBs: emptyDBs,
      addDb: addDb
    };
  }

  dbService.$inject = ['$http'];

  /* global angular */
  angular
    .module('wws')
    .service('dbService', dbService);
})();
