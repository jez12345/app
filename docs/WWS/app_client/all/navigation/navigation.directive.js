(function() {
  var navigation = function() {
    return {
      restrict: 'EA',
      templateUrl: '/all/navigation/navigation.html',
      controller: 'navigationCtrl',
      controllerAs: 'navvm'
    };
  };

  /* global angular */
  angular
    .module('wws')
    .directive('navigation', navigation);
})();
