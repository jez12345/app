(function() {
  function navigationCtrl($location, $route, authentication, dbService) {
    var navvm = this;
    var vm = this;

    navvm.currentLocation = $location.path();

    navvm.isLogedIn = authentication.isLogedIn();

    navvm.currentUser = authentication.currentUser();

    navvm.logout = function() {
      authentication.logout();
      $location.path('/');
      $route.reload();
    };

    vm.fillMe = function() {
      dbService
        .addDb()
        .then(
          function success(res) {
            console.log("BRAVO MALI" + res);
          },
          function error(res) {
            vm.formError = res.data;
          }
        );
    };


    vm.deleteDB = function() {
      dbService
        .emptyDBs()
        .then(
          function success(res) {
            console.log("BRAVO MALI" + res);
          },
          function error(res) {
            vm.formError = res.data;
          }
        );
    };

  }

  navigationCtrl.$inject = ['$location', '$route', 'authentication', 'dbService'];

  /* global angular */
  angular
    .module('wws')
    .controller('navigationCtrl', navigationCtrl);
})();
