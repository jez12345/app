(function() {
  function uniData($http, $routeParams) {

    var data = function() {
      return $http.get('/api/allUnis');
    };

    var addfeedback = function(data) {
      return $http.post('/api/feedback', data);
    }
    /*, {
      headers: {
        Authorization: 'Bearer ' + authentication.getToken()
      }
    }*/

    var addUni = function(data) {
      return $http.post('/api/newUni', data);
    };

    var idUni = function(data) {
      return $http.delete('/api/uni/' + data);
    };

    return {
      data: data,
      addfeedback: addfeedback,
      addUni: addUni,
      idUni: idUni
    };
  }

  uniData.$inject = ['$http', '$routeParams'];

  /* global angular */
  angular
    .module('wws')
    .service('uniData', uniData);
})();
