(function() {
  function usersData($http) {

    var data = function() {
      return $http.get('/api/allusers');
    };

    var userId = function(data) {
      alert("service userId " + data);
      return $http.delete('/api/myprofile/' + data);
    };

    return {
      data: data,
      userId: userId
    };
  }

  usersData.$inject = ['$http'];

  /* global angular */
  angular
    .module('wws')
    .service('usersData', usersData);
})();
