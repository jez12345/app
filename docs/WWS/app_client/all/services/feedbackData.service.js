(function() {
  function feedbackData($http) {

    var data = function() {
      return $http.get('/api/feedback');
    };

    var feedbackId = function(data) {
      return $http.delete('/api/feedback/' + data);
    };

    return {
      data: data,
      feedbackId: feedbackId
    };
  }

  feedbackData.$inject = ['$http'];

  /* global angular */
  angular
    .module('wws')
    .service('feedbackData', feedbackData);
})();
