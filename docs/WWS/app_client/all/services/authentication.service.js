(function() {
  function authentication($window, $http) {

    var b64Utf8 = function(niz) {
      return decodeURIComponent(Array.prototype.map.call($window.atob(niz), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
      }).join(''));
    };

    var setToken = function(token) {
      $window.localStorage['wws-token'] = token;
    };

    var getToken = function() {
      return $window.localStorage['wws-token'];
    };

    var register = function(user) {
      return $http.post('/api/register', user)
        .then(
          function success(res) {
            setToken(res.data.token);
          });
    };

    var login = function(user) {
      return $http.post('/api/login', user).then(
        function success(res) {
          setToken(res.data.token);
        });
    };

    var logout = function() {
      $window.localStorage.removeItem('wws-token');
    };

    var isLogedIn = function() {
      var token = getToken();
      if (token) {
        var tokenData = JSON.parse(b64Utf8(token.split('.')[1]));
        return tokenData.dateJWT > Date.now() / 1000;
      }
      else {
        return false;
      }
    };

    var currentUser = function() {
      if (isLogedIn()) {
        var token = getToken();
        var tokenData = JSON.parse(b64Utf8(token.split('.')[1]));
        return {
          fname: tokenData.fname,
          lname: tokenData.lname,
          email: tokenData.email,
          country: tokenData.country
        };
      }
    };

    return {
      setToken: setToken,
      getToken: getToken,
      register: register,
      login: login,
      logout: logout,
      isLogedIn: isLogedIn,
      currentUser: currentUser
    };
  }

  authentication.$inject = ['$window', '$http'];

  /* global angular */
  angular
    .module('wws')
    .service('authentication', authentication);
})();
