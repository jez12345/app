(function() {
  function feedbackCtrl($routeParams, $location, feedbackData, authentication) {
    var vm = this;

    vm.isLogedIn = authentication.isLogedIn();

    vm.indexPage = $location.path();

    feedbackData.data().then(
      function success(response) {
        vm.msg = response.data.length > 0 ? "" : "No comments.";
        vm.data = response.data;
      },
      function error(response) {
        vm.msg = "Error while fetching feedbacks.";
        console.log(response.e);
      });


    vm.feedbackDel = function(feedbackId) {
      alert("Dolazim li?")
      if (!feedbackId) {
        vm.formError = "There is something missing. Try it again!";
        alert("Cant find ID. ");
        return false;
      }
      else {
        vm.execDelId(feedbackId);
      }
    };

    vm.execDelId = function(feedbackId) {
      alert(feedbackId + "execDelId feedbackId");
      vm.formError = "";
      feedbackData
        .feedbackId(feedbackId)
        .then(
          function success(res) {
            console.log("BRAVO MALI" + res);
          },
          function error(res) {
            vm.formError = res.data.message;
          }
        );
    };

  }

  feedbackCtrl.$inject = ['$routeParams', '$location', 'feedbackData', 'authentication'];

  /* global angular */
  angular
    .module('wws')
    .controller('feedbackCtrl', feedbackCtrl);
})();
