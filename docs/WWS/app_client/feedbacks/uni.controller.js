(function() {
  function uniCtrl($routeParams, $location, $uibModal, uniData, authentication) {
    var vm = this;

    vm.isLogedIn = authentication.isLogedIn();

    vm.indexPage = $location.path();


    vm.modalOpen = function() {
      $uibModal.open({
        templateUrl: 'addUni.html',
        controller: 'addUniCtrl',
        controllerAs: 'vm'
      });
    };

    //Send data to render all unis
    uniData.data()
      .then(
        function success(response) {
          console.log(response.data);
          vm.msg = response.data.length > 0 ? "" : "No unis.";
          vm.data = response.data;
        },
        function error(response) {
          vm.msg = "Error while fetching unis.";
          console.log(response.e);
        });

    //Del one uni
    vm.deleteUni = function(uniId) {
      if (!uniId) {
        vm.formError = "There is something missing. Try it again!";
        alert("Cant find ID. deleteUni");
        return false;
      }
      else {
        vm.execDelId(uniId);
      }
    };

    vm.execDelId = function(uniId) {
      alert(uniId + "execDelId uniId");
      vm.formError = "";
      uniData
        .idUni(uniId)
        .then(
          function success(res) {
            console.log("BRAVO MALI" + res);
          },
          function error(res) {
            vm.formError = res.data.message;
          }
        );
    };

    //Drugi controller
    vm.feedData = {

      fname: "",
      lname: "",
      country: "",
      tuniversity: "",
      suniversity: "",
      body: ""
    };

    vm.sendData = function() {
      vm.err = "";
      if (!vm.feedData.user.fname || !vm.feedData.user.lname || !vm.feedData.user.country || !vm.feedData.tuniversity || !vm.feedData.suniversity || !vm.feedData.body) {
        alert("Ne prodjoh :(")
        vm.err = "Zahtevani so vsi podatki, prosim poskusite znova!";
        return false;
      }
      else {
        vm.registrate();
      }
    };

    vm.registrate = function() {
      vm.err = "";
      uniData
        .addfeedback(vm.feedData)
        .then(
          function success(res) {
            console.log("BRAVO!" + res);
          },
          function error(res) {
            vm.err = res;
          }
        );
    };
  }

  uniCtrl.$inject = ['$routeParams', '$location', '$uibModal', 'uniData', 'authentication'];

  /* global angular */
  angular
    .module('wws')
    .controller('uniCtrl', uniCtrl);
})();
