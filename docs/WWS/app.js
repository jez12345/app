require('dotenv').load();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var uglifyJs = require('uglify-js');
var fs = require('fs');

var combined = uglifyJs.minify({
  'app.js': fs.readFileSync('app_client/app.js', 'utf-8'),
  'feedback.controller.js': fs.readFileSync('app_client/feedbacks/feedback.controller.js', 'utf-8'),
  'feedbackData.service.js': fs.readFileSync('app_client/all/services/feedbackData.service.js', 'utf-8'),
  'uniData.service.js': fs.readFileSync('app_client/all/services/uniData.service.js', 'utf-8'),
  'users.service.js': fs.readFileSync('app_client/all/services/users.service.js', 'utf-8'),
  'authentication.service.js': fs.readFileSync('app_client/all/services/authentication.service.js', 'utf-8'),
  'uni.controller.js': fs.readFileSync('app_client/feedbacks/uni.controller.js', 'utf-8'),
  'users.controller.js': fs.readFileSync('app_client/users/users.controller.js', 'utf-8'),
  'navigation.directive.js': fs.readFileSync('app_client/all/navigation/navigation.directive.js', 'utf-8'),
  'navigation.controller.js': fs.readFileSync('app_client/all/navigation/navigation.controller.js', 'utf-8'),
  'navigation.service.js': fs.readFileSync('app_client/all/navigation/navigation.service.js', 'utf-8'),
  'addUni.controller.js': fs.readFileSync('app_client/addUni.controller.js', 'utf-8'),
  'login.controller.js': fs.readFileSync('app_client/authentication/login.controller.js', 'utf-8'),
  'register.controller.js': fs.readFileSync('app_client/authentication/register.controller.js', 'utf-8'),
  'myprofile.controller.js': fs.readFileSync('app_client/users/myprofile.controller.js', 'utf-8')
});

fs.writeFile('public/angular/wwsfri.min.js', combined.code, function(error) {
  if (error)
    console.log(error);
  else
    console.log('Script is created and saved in "wwsfri.min.js".' + combined.code);
});


var passport = require('passport');

require('./app_api/models/db');
require('./app_api/config/passport');


//var indexRouter = require('./app_server/routes/index');
var indexApi = require('./app_api/routes/index');
var usersRouter = require('./app_server/routes/users');

var app = express();

// Odprava varnostnih pomanjkljivosti
app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'DENY');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'app_server', 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(passport.initialize());

//app.use('/', indexRouter);
app.use('/api', indexApi);
app.use('/users', usersRouter);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// Obvladovanje napak zaradi avtentikacije
app.use(function(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({
      "message!": err.name + ": " + err.message
    });
  }
});


// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
